# Konwersja Bayer -> RGB na FPGA

## Algorytmy konwersji

W projekcie wykonane zostaną 3 algorytmy konwersji:
- Nearest Neighbour
- Bi-linear interpolation

Jeden z:
- Smooth hue interpolation
- Edge sensing

## Porównanie

### Porównanie jakości obrazu

- Moire pattern
- Zipper effect
- False color
- peak signal-to-noise ratio

### Porównanie IP-core

- Wykorzystanie % FPGA (LUT,BRAM,etc.)
- Fmax

## Realizacja

### Platforma HW: Digilent Zybo

### Software

#### Linux

https://gitlab.com/fpga_debayer/linux/tree/msobkowski/zynq_demosaicker

Moduł jadra do kontrolowania układu:

'drivers/media/platform/zynq_demosaicker/zynq_demosaicker.c'

- Czeka na dane wpisywane do pliku znakowe (chardev)
- Transfer danych przy użyciu Xilinx AXI VDMA do bloku debayer

#### Narzędzia

draw - program przetwarzający obrazy w róznych formatach (raw, yuv, rgb) na plik bmp.
rgb2bayer - program symulujacy kamerę z filtrem bayera, z obrazu wejściowego zachowuje tylko konkretne składowe dla danego piksela.
loadpng - program konwertujący plik z formatu png na surowe dane rgb888

## Układ na FPGA

### Demosaicker

![projekt](axi_stream_pipeline.png)

Układ zrealizowany w języku Chisel, poszczególne bloki wydzielone jako osobne klasy, z dodatkową klasą Top
realizującą połączenia między blokami.

Źródło: https://gitlab.com/fpga_debayer/linux/tree/msobkowski/linebuffer_chisel

Cały blok kompilowany do jednego pliku Verilog, importowanego do Vivado.

### Projekt Vivado

https://gitlab.com/fpga_debayer/fpga_debayer_vivado

Schemat blokowy projektu:

![projekt](vivado.png)

### Testowanie

Testy napisane przy użyciu klasy PeekPokeTester w Chiselu - wykonywane przy użyciu symulatora Verilator.

Wynikowe pliki vcd analizowane w programie gtkwave.

Przykładowa symulacja bufora linii:

![projekt](gtkwave.png)

Cały układ demosaickera testowany poprzez załadowanie pliku z obrazem w formacie Bayer do układu i zapisaniu wyjsciowych danych do pliku wynikowego.

Przykładowy test:

obraz wejściowy:

![projekt](stage1.png)

symulowany obraz w formacie bayer:

![projekt](stage2.png)

wynikowy obraz po przetworzeniu:

![projekt](stage3.png)

